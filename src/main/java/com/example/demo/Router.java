package com.example.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@RestController
public class Router {

    @RequestMapping(value = "/")
    public String root() {
        return "hello";
    }
}
