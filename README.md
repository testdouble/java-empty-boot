# Empty Spring Boot Java Application

## Requirements

- JDK 14.1
- Java IDE

## Development

Start Server  
http://localhost:8080

```
./gradlew bootStart
```

Run Tests

```
./gradlew test
```

## Docs

- [Spring RESTful Web Services](https://spring.io/guides/gs/rest-service/)
- [Testing Spring Web Layer](https://spring.io/guides/gs/testing-web/)